package com.atlassian.jira.plugins.dnd.attachment.providers;

import com.atlassian.core.util.thumbnail.ThumbnailUtil;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.json.marshal.wrapped.JsonableString;
import com.atlassian.webresource.api.data.WebResourceDataProvider;
import com.google.common.base.Joiner;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;

public class ThumbnaillMimeTypesProvider implements WebResourceDataProvider
{
    @Override
    public Jsonable get()
    {
        List<String> allThumbnailTypes = ThumbnailUtil.getThumbnailMimeTypes();
        // Tiff images aren't displayable in most web browsers, so let's remove them from here.
        List<String> supportedThumbnailTypes = newArrayList(allThumbnailTypes);
        supportedThumbnailTypes.remove("image/x-tiff");
        supportedThumbnailTypes.remove("image/tiff");
        return new JsonableString(Joiner.on(",").join(supportedThumbnailTypes));
    }
}
