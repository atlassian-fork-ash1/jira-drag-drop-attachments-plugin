AJS.test.require([
    "jira.webresources:require-shim",
    "jira.webresources:jira-global",
    "com.atlassian.auiplugin:dialog2",
    "com.atlassian.jira.plugins.jira-dnd-attachment-plugin:dnd-create-issue-drop-zone"
], function(){
    "use strict";

    var $ = require('jquery');
    var AttachFilesDropZone = require('dndattachment/dropzones/AttachFilesDropZone');

    module("AttachFilesDropZone", {
        setup: function() {
            this.sandbox = sinon.sandbox.create();
            JIRA.Events.LOCK_PANEL_REFRESHING = "lockPanelRefreshing";
            JIRA.Events.REFRESH_ISSUE_PAGE = "refreshIssuePage";
        },

        teardown: function() {
            this.sandbox.restore();
            delete JIRA.Events.LOCK_PANEL_REFRESHING;
            delete JIRA.Events.REFRESH_ISSUE_PAGE;
        }
    });

    test("AttachFilesDropZone constructor calls render", function() {
        var $fixture = $('#qunit-fixture');
        var node$ = $('<div></div>').appendTo($fixture);
        this.sandbox.stub(JIRA, "trigger");
        this.sandbox.stub(JIRA.SmartAjax, "makeRequest").returns(new $.Deferred());
        this.sandbox.spy(AttachFilesDropZone.prototype, "render");

        var instance = new AttachFilesDropZone($fixture);

        ok(AttachFilesDropZone.prototype.render.calledOnce, "render called");
        ok(JIRA.SmartAjax.makeRequest.callCount == 0, "makeRequest not called");
    });

    test("AttachFilesDropZone commitUpload markDirty test", function() {
        var instance = new (Class.extend({
            markDirty: this.sandbox.stub(),
            queueEvent: this.sandbox.stub()
        }))();

        AttachFilesDropZone.prototype.commitUpload.call(instance, []);
        ok(!instance.markDirty.calledWith(false), 'markDirty not called with false');
    });
});
