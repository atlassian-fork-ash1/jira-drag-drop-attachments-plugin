AJS.test.require([
    "jira.webresources:require-shim",
    "jira.webresources:jira-global",
    "com.atlassian.jira.plugins.jira-dnd-attachment-plugin:drag-and-drop-attachment-javascript"
], function(){
    "use strict";

    var $ = require('jquery');
    var AttachmentsDropZone = require('dndattachment/dropzones/AttachmentsDropZone');
    var Events = require('jira/util/events');
    var SmartAjax = require('jira/ajs/ajax/smart-ajax');

    module("AttachmentsDropZone", {
        setup: function() {
            this.sandbox = sinon.sandbox.create();
            JIRA.Issues = {Api:{
                getSelectedIssueId: function() {
                    return 12345;
                }
            }};
        },

        teardown: function() {
            this.sandbox.restore();
            delete JIRA.Issues;
        }
    });

    test("AttachmentsDropZone commitUpload test", function(assert) {
        var done = assert.async();
        var $fixture = $('#qunit-fixture');
        var requestDeferred = new $.Deferred();
        this.sandbox.stub(SmartAjax, "makeRequest").returns(requestDeferred);
        this.sandbox.stub(Events, "trigger");

        var instance = new (Class.extend({
            $node: $fixture,
            queueTask: this.sandbox.stub(),
            queueEvent: this.sandbox.stub(),
            attachFile: AttachmentsDropZone.prototype.attachFile,
            getViewMode: function() {
                return "gallery"
            }
        }));

        var files = [1,2,3];
        var attachments = [];

        var commit = AttachmentsDropZone.prototype.commitUpload.call(instance, files);
        ok(commit != null, "commit is not null");
        ok(SmartAjax.makeRequest.calledOnce, "makeRequest called once");

        var args = SmartAjax.makeRequest.getCall(0).args[0];
        ok(args.data.filetoconvert.join() == files.join(), 'fileIDs in request');

        requestDeferred.resolve(attachments);
        commit.then(function() {
            ok(Events.trigger.calledOnce, "trigger called once");
            done();
        });
    });

    test("AttachmentsDropZone commitUpload markDirty test", function() {
        var $fixture = $('#qunit-fixture');
        var instance = new (AttachmentsDropZone.extend({
            $node: $fixture,
            pendingQueue: [],
            markDirty: this.sandbox.stub(),
            queueTask: AttachmentsDropZone.prototype.queueTask
        }))();

        var requestDeferred = new $.Deferred();
        this.sandbox.stub(SmartAjax, "makeRequest").returns(requestDeferred);
        this.sandbox.stub(JIRA, "trigger");

        AttachmentsDropZone.prototype.commitUpload.call(instance, []);
        ok(instance.markDirty.calledWith(true), 'markDirty called with true');

        var args = SmartAjax.makeRequest.getCall(0).args[0];

        var attachments = [];

        requestDeferred.resolve(attachments);
        ok(instance.markDirty.calledWith(false), 'markDirty called with false');
    });

    test("AttachmentsDropZone insert progress bar should respect sorting", function() {
        var $fixture = $('#qunit-fixture');
        var $node = $('<div><div></div><ul id="file_attachments" data-sort-key="fileName" data-sort-order="asc"></ul></div>').appendTo($fixture);
        var $listNode = $node.find('#file_attachments');

        var $attachment1 = $('<li><span class="attachment-title">AAA</span></li>').appendTo($listNode);
        var $attachment2 = $('<li><span class="attachment-title">CCC</span></li>').appendTo($listNode);

        var $progressBar = $('<li><span class="attachment-title">BBB</span></li>');

        var attachmentsDropZone = new AttachmentsDropZone($node.find('div')[0]);
        attachmentsDropZone.render();
        attachmentsDropZone.insertProgressBar($progressBar);

        ok($progressBar.prev().is($attachment1), "attachment1 is previous one");
        ok($progressBar.next().is($attachment2), "attachment2 is next one");
    });
});
