AJS.test.require([
    "jira.webresources:require-shim",
    "jira.webresources:jira-global",
    "com.atlassian.jira.plugins.jira-dnd-attachment-plugin:drag-and-drop-attachment-javascript"
], function () {
    "use strict";

    var UploadHandler = require('dndattachment/upload/handler');
    var DefaultExecutor = require('dndattachment/upload/default/executor');
    var _ = require('underscore');
    var $ = require('jquery');
    var Dialog = require('jira/dialog/dialog');
    var Utility = require('dndattachment/ctrlv/utility');

    module("UploadHandler", {
        setup: function () {
            this.sandbox = sinon.sandbox.create();
        },

        teardown: function () {
            this.sandbox.restore();
        },

        createMockExecutor: function (result) {
            return {
                isValid: sinon.stub().returns(true),
                processFiles: sinon.stub().returns(result),
                weight: 0,
                name: 'mock executor'
            };
        }
    });

    var setupTest = function (UploadHandler, DefaultExecutor) {
        // Init the handler so it's listening
        UploadHandler.initialize();
        UploadHandler.registerExecutor(DefaultExecutor);
    };

    var teardownTest = function (UploadHandler, DefaultExecutor) {
        // Disable the handler
        UploadHandler.unregisterExecutor(DefaultExecutor);
        UploadHandler.disable();
    };

    test("registerExecutor validation test", function (assert) {
        var validExecutor = {
            name: "Dummy",
            weight: 10, // higher than the default
            isValid: function () {
                return false;
            },
            processFiles: function () {
                return null;
            }
        };
        assert.equal(UploadHandler.registerExecutor(validExecutor), true);

        var nonameExecutor = {
            weight: 10, // higher than the default
            isValid: function () {
                return false;
            },
            processFiles: function () {
                return null;
            }
        };
        assert.equal(UploadHandler.registerExecutor(nonameExecutor), false);

        var noweightExecutor = {
            name: "Dummy",
            isValid: function () {
                return false;
            },
            processFiles: function () {
                return null;
            }
        };
        assert.equal(UploadHandler.registerExecutor(noweightExecutor), false);

        var noisValidExecutor = {
            name: "Dummy",
            weight: 10, // higher than the default
            processFiles: function () {
                return null;
            }
        };
        assert.equal(UploadHandler.registerExecutor(noisValidExecutor), false);

        var noProcessFilesExecutor = {
            name: "Dummy",
            weight: 10, // higher than the default
            isValid: function () {
                return false;
            }
        };
        assert.equal(UploadHandler.registerExecutor(noProcessFilesExecutor), false);

        var emptyExecutor = {};
        assert.equal(UploadHandler.registerExecutor(emptyExecutor), false);
    });

    test("registerExecutor valid executor is not called when isValid returns false", function (assert) {
        setupTest(UploadHandler, DefaultExecutor);

        //Set up the test
        var Events = require('jira/util/events');
        var EventTypes = require('dndattachment/util/events/types');
        var dummyProcessFiles = sinon.stub();
        var defaultProcessFiles = sinon.stub();
        var result = new $.Deferred();

        var dummyExecutor = {
            name: "Dummy",
            weight: 10, // higher than the default
            isValid: function () {
                return false;
            },
            processFiles: function () {
                dummyProcessFiles();
                return result;
            }
        };

        var fakeAttachmentDropZone = {
            uploadFiles: function () {
                defaultProcessFiles();
                return result;
            }
        };
        UploadHandler.setAttachmentDropZone(fakeAttachmentDropZone);
        // Attach the dummy executor
        UploadHandler.registerExecutor(dummyExecutor);

        // Throw an event and hope it fires
        Events.trigger(EventTypes.ATTACHMENT_FOR_PAGE_RECEIVED, {
            files: [1, 2, 3]
        });

        result.resolve().always(function () {
            sinon.assert.notCalled(dummyProcessFiles, "the dummy was not called");
            sinon.assert.calledOnce(defaultProcessFiles, "the default was called");

            UploadHandler.unregisterExecutor(dummyExecutor);
            teardownTest(UploadHandler, DefaultExecutor);
        });
    });

    test("registerExecutor valid executor is called when isvalid returns true", function (assert) {
        setupTest(UploadHandler, DefaultExecutor);

        //Set up the test
        var Events = require('jira/util/events');
        var EventTypes = require('dndattachment/util/events/types');
        var dummyProcessFiles = sinon.stub();
        var defaultProcessFiles = sinon.stub();
        var result = new $.Deferred();

        var dummyExecutor = {
            name: "Dummy",
            weight: 10, // higher than the default
            isValid: function () {
                return true;
            },
            processFiles: function () {
                dummyProcessFiles();
                return result;
            }
        };

        var fakeAttachmentDropZone = {
            uploadFiles: function () {
                defaultProcessFiles();
                return result;
            }
        };
        UploadHandler.setAttachmentDropZone(fakeAttachmentDropZone);
        // Attach the dummy executor
        UploadHandler.registerExecutor(dummyExecutor);

        // Throw an event and hope it fires
        Events.trigger(EventTypes.ATTACHMENT_FOR_PAGE_RECEIVED, {
            files: [1, 2, 3]
        });

        result.resolve().always(function () {
            sinon.assert.calledOnce(dummyProcessFiles, "The dummy was called once");
            sinon.assert.notCalled(defaultProcessFiles, "the default was not called");

            UploadHandler.unregisterExecutor(dummyExecutor);
            teardownTest(UploadHandler, DefaultExecutor);
        });
    });

    test("handleAttachmentReceived success test", function (assert) {
        setupTest(UploadHandler, DefaultExecutor);

        //Set up the test
        var Events = require('jira/util/events');
        var EventTypes = require('dndattachment/util/events/types');

        var success = sinon.stub();
        var failure = sinon.stub();

        var result = new $.Deferred();

        var fakeAttachmentDropZone = {
            uploadFiles: function () {
                return result;
            }
        };

        UploadHandler.setAttachmentDropZone(fakeAttachmentDropZone);
        // Throw an event and hope it fires
        Events.trigger(EventTypes.ATTACHMENT_FOR_PAGE_RECEIVED, {
            files: [1, 2, 3],
            successCallback: success,
            failureCallback: failure
        });

        result.resolve().always(function () {
            sinon.assert.calledOnce(success, "success method was called once");
            sinon.assert.notCalled(failure, "failure method was not called");

            teardownTest(UploadHandler, DefaultExecutor);
        });
    });

    test("handleAttachmentReceived failure test", function (assert) {
        setupTest(UploadHandler, DefaultExecutor);

        //Set up the test
        var Events = require('jira/util/events');
        var EventTypes = require('dndattachment/util/events/types');

        var success = sinon.stub();
        var failure = sinon.stub();

        var result = new $.Deferred();

        var fakeAttachmentDropZone = {
            uploadFiles: function () {
                return result;
            }
        };

        UploadHandler.setAttachmentDropZone(fakeAttachmentDropZone);
        // Throw an event and hope it fires
        Events.trigger(EventTypes.ATTACHMENT_FOR_PAGE_RECEIVED, {
            files: [1, 2, 3],
            successCallback: success,
            failureCallback: failure
        });

        result.reject().always(function () {
            sinon.assert.notCalled(success, "success method was not once");
            sinon.assert.calledOnce(failure, "failure method was called once");

            teardownTest(UploadHandler, DefaultExecutor);
        });
    });

    test("should use executors when Dialog without $form is present", function () {
        //Set up the test
        var result = new $.Deferred();
        var MockExecutor = this.createMockExecutor(result);
        setupTest(UploadHandler, MockExecutor);
        this.sandbox.stub(Utility, 'dropFileToElement');
        var Events = require('jira/util/events');
        var EventTypes = require('dndattachment/util/events/types');
        Dialog.current = {};

        // Throw an event and hope it fires
        Events.trigger(EventTypes.ATTACHMENT_FOR_PAGE_RECEIVED, {
            files: [1, 2, 3]
        });

        sinon.assert.notCalled(Utility.dropFileToElement);
        sinon.assert.calledOnce(MockExecutor.processFiles, "Executor should be used");

        teardownTest(UploadHandler, MockExecutor);
        Dialog.current = null;
    });

    test("should use Utility.dropFileToElement when Dialog with $form is present", function () {
        //Set up the test
        var result = new $.Deferred();
        var MockExecutor = this.createMockExecutor(result);
        setupTest(UploadHandler, MockExecutor);
        this.sandbox.stub(Utility, 'dropFileToElement');
        this.sandbox.stub(Utility, 'createBlobFromFile');
        var Events = require('jira/util/events');
        var EventTypes = require('dndattachment/util/events/types');
        Dialog.current = {$form: {}};

        // Throw an event and hope it fires
        Events.trigger(EventTypes.ATTACHMENT_FOR_PAGE_RECEIVED, {
            files: [1, 2, 3]
        });

        sinon.assert.calledOnce(Utility.dropFileToElement);
        sinon.assert.notCalled(MockExecutor.processFiles, "Executor shouldn't be called");

        teardownTest(UploadHandler, MockExecutor);
        Dialog.current = null;
    });
});
