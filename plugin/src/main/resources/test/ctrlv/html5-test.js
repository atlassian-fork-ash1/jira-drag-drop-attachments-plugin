AJS.test.require(["com.atlassian.jira.plugins.jira-dnd-attachment-plugin:jira-html5-attach-images-resources"], function() {
    "use strict";

    var $ = require('jquery');

    module("JIRA.AttachImagesPlugin", {
        setup: function () {
            this.sandbox = sinon.sandbox.create();
        },
        teardown: function () {
            this.sandbox.restore();
        }
    });

    test("Test Validation For Good Data", function () {
        var content = ["<a id=\"a\"><b id=\"b\">hey!<\/b><\/a>"];
        var file = content; // the blob
        var fileName = "buggy-image.png";

        // Mocking view methods
        var html5 = require("dndattachment/ctrlv/html5");
        this.dialogViewMock = this.sandbox.mock(html5.dialogView);
        this.dialogViewMock.expects("getFileSize").once().returns(100);
        this.dialogViewMock.expects("getMaxSize").once().returns(1024);

        var errors = html5.validateFormData(file, fileName);
        ok($.isEmptyObject(errors), "fileName and fileUpload are good");
    });

    test("Test Validation For Empty File and File Name", function () {
        var file = {};
        var fileName = "";

        var html5 = require("dndattachment/ctrlv/html5");
        this.dialogViewMock = this.sandbox.mock(html5.dialogView);
        this.dialogViewMock.expects("buildPasteCatcher").once().returns({});
        html5.initScreenshotPasteHandler();

        var errors = html5.validateFormData(file, fileName);
        ok("fileName" in errors, "fileName is illegal");
        ok("fileUpload" in errors, "fileUpload is illegal");
    });

    test("Test Validation For Empty File and Good File Name", function () {
        var file = {};
        var fileName = "attachment.png";

        var html5 = require("dndattachment/ctrlv/html5");
        this.dialogViewMock = this.sandbox.mock(html5.dialogView);
        this.dialogViewMock.expects("buildPasteCatcher").once().returns({});
        html5.initScreenshotPasteHandler();
        var errors = html5.validateFormData(file, fileName);
        ok(!("fileName" in errors), "fileName is is good");
        ok("fileUpload" in errors, "fileUpload is illegal");
    });

    test("Test Validation For Good File and Empty File Name", function () {
        var content = ["<a id=\"a\"><b id=\"b\">hey!<\/b><\/a>"];
        var file = content; // the blob
        var fileName = "";

        // Mocking view methods
        var html5 = require("dndattachment/ctrlv/html5");
        this.dialogViewMock = this.sandbox.mock(html5.dialogView);
        this.dialogViewMock.expects("getFileSize").once().returns(100);
        this.dialogViewMock.expects("getMaxSize").once().returns(1024);


        var errors = html5.validateFormData(file, fileName);
        ok("fileName" in errors, "fileName is is illegal");
        ok(!("fileUpload" in errors), "fileUpload is good");
    });

    test("Test Validation For Good File and Illegal chars in File Name", function () {
        var content = ["<a id=\"a\"><b id=\"b\">hey!<\/b><\/a>"];
        var file = content; // the blob
        var fileName = "<>>>!";

        // Mocking view methods
        var html5 = require("dndattachment/ctrlv/html5");
        this.dialogViewMock = this.sandbox.mock(html5.dialogView);
        this.dialogViewMock.expects("getFileSize").once().returns(100);
        this.dialogViewMock.expects("getMaxSize").once().returns(1024);


        var errors = html5.validateFormData(file, fileName);
        ok("fileName" in errors, "fileName is is illegal");
        ok(!("fileUpload" in errors), "fileUpload is good");
    });

    test("Test PNG Extension validation", function () {

        var html5 = require("dndattachment/ctrlv/html5");
        ok(html5.dialogView.hasPngExtension("screenshot-1.png"), "Must recognize PNG extension");
        ok(html5.dialogView.hasPngExtension("screenshot-1.PNG"), "Must recognize PNG extension");
        ok(html5.dialogView.hasPngExtension("screenshot-1.PNG.PNG"), "Must recognize PNG extension");

        ok(!(html5.dialogView.hasPngExtension("screenshot-1-PNG")), "Must NOT recognize PNG extension");
        ok(!(html5.dialogView.hasPngExtension("screenshot-1.PNG.JPG")), "Must NOT recognize PNG extension");
        ok(!(html5.dialogView.hasPngExtension("PNG.screenshot-1")), "Must NOT recognize PNG extension");
    });

    test("Test Paste Catcher should be rendered outside viewport", function() {
        var $container = $("<div>");
        this.sandbox.stub(document, "getElementById").withArgs("attach-screenshot-form").returns($container[0]);

        var html5 = require("dndattachment/ctrlv/html5");
        var $pasteCatcher = $(html5.dialogView.buildPasteCatcher());

        equal($pasteCatcher.css("position"), "absolute", "content editable div must have position absolute");
        notEqual($pasteCatcher.css("top"), "auto", "content editable div must not have auto as top property");
    });

    test("Test JIRA.ScreenshotDialog exists as a public API see SW-306", function () {

        //SW-306 js errors are present when this reference isn't available in global scope
        ok(JIRA.ScreenshotDialog);
    });
});
