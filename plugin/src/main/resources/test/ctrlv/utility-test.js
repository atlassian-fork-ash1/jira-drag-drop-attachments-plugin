AJS.test.require(["com.atlassian.jira.plugins.jira-dnd-attachment-plugin:jira-html5-attach-images-resources"], function() {
    "use strict";

    var Navigator = require("jira/util/navigator");
    var moment = require("jira/moment");
    var $ = require("jquery");
    var utility = require("dndattachment/ctrlv/utility");

    module("dndattachment/ctrlv/utility", {
        pasteEvents: {
            mac: {
                metaKey: true,
                which: 86,
                ctrlKey: false
            },

            pc: {
                metaKey: false,
                which: 86,
                ctrlKey: true
            },
            noImage: {
                clipboardData: {
                    items: [
                        {type: "text"},
                        {type: "rtf"}
                    ]
                }
            },
            imageNoRtf: {
                clipboardData: {
                    items: [
                        {type: "text"},
                        {type: "image"}
                    ]
                }
            },
            imageRtf: {
                clipboardData: {
                    items: [
                        {type: "text"},
                        {type: "rtf"},
                        {type: "image"}
                    ]
                }
            }
        },
        setup: function () {
            this.sandbox = sinon.sandbox.create();
            this.context = AJS.test.mockableModuleContext();
            this.context.mock("jira/moment", moment);
        },
        teardown: function () {
            this.sandbox.restore();
        }
    });

    ["Macintosh", "MacIntel", "MacPPC", "Mac68K"].forEach(function (platform) {
        test("Only ⌘+v should be a paste event on " + platform, function () {
            this.sandbox.stub(Navigator, "isMac").returns(false);
            equal(utility.isKeyPasteEvent(this.pasteEvents.mac, platform), true);
            equal(utility.isKeyPasteEvent(this.pasteEvents.pc, platform), false);
        });
    });


    [
        "OS/2", "Pocket PC", "Windows", "Win16", "Win32", "WinCE",
        "SunOS", "SunOS i86pc", "SunOS sun4u",
        "Linux", "Linux i686", "Linux i686 X11", "Linux armv7l", "Linux x86_64 X11"
    ].forEach(function (platform) {
          test("Only Ctrl+v should be a paste event on " + platform, function () {
              this.sandbox.stub(Navigator, "isMac").returns(false);
              equal(utility.isKeyPasteEvent(this.pasteEvents.pc, platform), true);
              equal(utility.isKeyPasteEvent(this.pasteEvents.mac, platform), false);
          });
      });

    test("dropFileToElement should return false when dropHandled not triggered", function() {
        var file = {};
        var $fixture = $("#qunit-fixture");
        var time = this.sandbox.stub().returns({format: this.sandbox.stub()});

        this.context.mock("dndattachment/ctrlv/time", time);

        var utility_mocked = this.context.require("dndattachment/ctrlv/utility");

        equal(utility_mocked.dropFileToElement(file, $fixture), false);
    });

    test("dropFileToElement should return true when dropHandled was triggered", function() {
        var file = {};
        var $fixture = $("#qunit-fixture");

        this.stub(utility, "isKeyPasteEvent").withArgs(this.keyPasteEvent).returns(true);
        var time = this.sandbox.stub().returns({format: this.sandbox.stub()});

        this.context.mock("dndattachment/ctrlv/time", time);

        var utility_mocked = this.context.require("dndattachment/ctrlv/utility");

        $fixture.one("drop", function() {
            $(document).trigger("dropHandler");
        });
        equal(utility_mocked.dropFileToElement(file, $fixture), false);
    });

    test("Thumbnails allowed by default if not specified in metadata", function() {
        strictEqual(utility.isThumbnailsAllowed(), true);
    });

    test("Thumbnails allowed by default if specified in metadata as true", function() {
        $('#qunit-fixture').append('<div id="dnd-metadata-webpanel" data-thumbnails-allowed="true"></div>');

        strictEqual(utility.isThumbnailsAllowed(), true);
    });

    test("Thumbnails allowed by default if specified in metadata as truthy", function() {
        $('#qunit-fixture').append('<div id="dnd-metadata-webpanel" data-thumbnails-allowed="99"></div>');

        strictEqual(utility.isThumbnailsAllowed(), true);
    });

    test("Thumbnails not allowed by default if specified in metadata as false", function() {
        $('#qunit-fixture').append('<div id="dnd-metadata-webpanel" data-thumbnails-allowed="false"></div>');

        strictEqual(utility.isThumbnailsAllowed(), false);
    });

    test("Thumbnails not allowed by default if specified in metadata as falsy", function() {
        $('#qunit-fixture').append('<div id="dnd-metadata-webpanel" data-thumbnails-allowed="0"></div>');

        strictEqual(utility.isThumbnailsAllowed(), false);
    });

    test("Gifs should be inserted as wiki markup without |thumbnail", function() {
        equal(utility.getMarkup("image.gif"), "!image.gif!");
    });

    test("Bmps should be inserted as wiki markup without |thumbnail", function() {
        equal(utility.getMarkup("image.bmp"), "!image.bmp!");
    });

    test("Images should be inserted as wiki markup with |thumbnail", function() {
        equal(utility.getMarkup("image.png"), "!image.png|thumbnail!");
        equal(utility.getMarkup("image.pjpeg"), "!image.pjpeg|thumbnail!");
        equal(utility.getMarkup("image.jpeg"), "!image.jpeg|thumbnail!");
        equal(utility.getMarkup("image.jpg"), "!image.jpg|thumbnail!");
    });

    test("Images should be inserted as wiki markup with |thumbnail if explicitly allowed", function() {
        $('#qunit-fixture').append('<div id="dnd-metadata-webpanel" data-thumbnails-allowed="true"></div>');

        equal(utility.getMarkup("image.png"), "!image.png|thumbnail!");
        equal(utility.getMarkup("image.pjpeg"), "!image.pjpeg|thumbnail!");
        equal(utility.getMarkup("image.jpeg"), "!image.jpeg|thumbnail!");
        equal(utility.getMarkup("image.jpg"), "!image.jpg|thumbnail!");
    });

    test("Images should be inserted as wiki markup without |thumbnail if explicitly disallowed", function() {
        $('#qunit-fixture').append('<div id="dnd-metadata-webpanel" data-thumbnails-allowed="false"></div>');

        equal(utility.getMarkup("image.png"), "!image.png!");
        equal(utility.getMarkup("image.pjpeg"), "!image.pjpeg!");
        equal(utility.getMarkup("image.jpeg"), "!image.jpeg!");
        equal(utility.getMarkup("image.jpg"), "!image.jpg!");
    });

    test("All other attachments should be inserted as wiki markup [^..]", function() {
        equal(utility.getMarkup("doc.doc"), "[^doc.doc]");
        equal(utility.getMarkup("numbers.xls"), "[^numbers.xls]");
        equal(utility.getMarkup("imgjpg.jpg.bmp.tiff.gif.somethingelse"), "[^imgjpg.jpg.bmp.tiff.gif.somethingelse]");
    });

    test("isWikiTextfield correctly recognises wiki textfields", function() {
        var $wikiTextfield = $("<textarea class=\"wiki-textfield\"></textarea>");
        var $fakeWikiTextfield = $("<div class=\"wiki-textfield\"></div>");
        equal(utility.isWikiTextfield($wikiTextfield), true);
        equal(utility.isWikiTextfield($fakeWikiTextfield), false);
    });

    test("insertToInput rejects changing the input value if an empty string is passed in", function() {
        var $validField = $("<input id='summary' value='not-changed'>");

        utility.insertToInput("", $validField, null, null, null);

        strictEqual($validField.val(), "not-changed");
    });

    test("insertToInput rejects changing the input value if a null content is passed in", function() {
        var $validField = $("<input id='summary' value='not-changed'>");

        utility.insertToInput(null, $validField, null, null, null);

        strictEqual($validField.val(), "not-changed");
    });

    test("insertToInput rejects changing the value of an element other than input with id set as summary", function() {
        var $field = $("<textarea id='summary'>textarea value</textarea>");

        utility.insertToInput("content", $field, null, null, null);

        strictEqual($field.val(), "textarea value");
    });

    test("insertToInput rejects changing the value of an input that does not match the exact id", function() {
        var $field = $("<input id=\"not-summary\" value='not-changed'>");

        utility.insertToInput("content", $field, null, null, null);

        strictEqual($field.val(), "not-changed");
    });

    test("insertToInput updates the value of a valid target field", function() {
        var $validField = $("<input id='summary' value='original-value'>");

        utility.insertToInput("custom-nice-content", $validField, 0, 100, true);

        strictEqual($validField.val(), "custom-nice-content");
    });

    test("getTextContent should check Navigator", function() {
        var mockNavigator = {
            isIE: this.sandbox.stub().returns(true)
        };
        this.context.mock("jira/util/navigator", mockNavigator);

        var textStub = this.sandbox.stub().returns("$.text");
        var mock$ = function() {
            return {
                text: textStub
            }
        };
        mock$.inArray = $.inArray.bind($);
        this.context.mock("jquery", mock$);

        var node = {
            innerText: "innerText",
            textContent: "textContent"
        };

        var utility_mocked = this.context.require("dndattachment/ctrlv/utility");
        strictEqual(utility_mocked.getTextContent(node), "innerText");

        mockNavigator.isIE.returns(false);
        strictEqual(utility_mocked.getTextContent(node), "$.text");
        strictEqual(utility_mocked.getTextContent(null), "");
    });

    test("isImagePasteEvent returns false if there is no image among items in the event", function() {
        strictEqual(utility.isImagePasteEvent(this.pasteEvents.noImage), false);
    });

    test("isImagePasteEvent returns true if there is image and no rtf among items in the event", function() {
        strictEqual(utility.isImagePasteEvent(this.pasteEvents.imageNoRtf), true);
    });

    test("isImagePasteEvent returns false if there is image and rtf among items in the event", function() {
        strictEqual(utility.isImagePasteEvent(this.pasteEvents.imageRtf), false);
    });

    test("dragEventContainsFiles returns true if it cannot determine whether files are being dragged", function() {
        this.stub(Navigator, "isMozilla").returns(false);
        var event = {};

        ok(utility.dragEventContainsFiles(event));
    });

    test("dragEventContainsFiles returns true if dataTransfer.types is an array and contains 'Files'", function() {
        this.stub(Navigator, "isMozilla").returns(false);
        var event = {
            dataTransfer: {
                types: ["Files"]
            }
        };

        ok(utility.dragEventContainsFiles(event));
    });

    test("dragEventContainsFiles returns false if dataTransfer.types is an array and does not contain 'Files'", function() {
        this.stub(Navigator, "isMozilla").returns(false);
        var event = {
            dataTransfer: {
                types: ["Not-Files"]
            }
        };

        ok(!utility.dragEventContainsFiles(event));
    });

    var fakeArray = function (arr) {
        var obj = {};
        for (var i in arr) {
            if (arr.hasOwnProperty(i)) {
                obj[i] = arr[i];
            }
        }

        return obj;
    };


    test("dragEventContainsFiles returns true in firefox if types contains 'application/x-moz-file'", function() {
        this.stub(Navigator, "isMozilla").returns(true);

        var event = {
            dataTransfer: {
                // event.dataTransfer.types may contain an array-like object that have no 'indexOf' method
                types: fakeArray(['application/x-moz-file', 'Files'])
            }
        };

        ok(utility.dragEventContainsFiles(event));
    });

    test("dragEventContainsFiles returns false in firefox if types does not contain 'application/x-moz-file'", function() {
        this.stub(Navigator, "isMozilla").returns(true);
        var event = {
            dataTransfer: {
                types: fakeArray(['Files'])
            }
        };

        ok(!utility.dragEventContainsFiles(event));
    });
});
