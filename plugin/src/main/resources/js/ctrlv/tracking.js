define("dndattachment/ctrlv/tracking", ['require', 'exports'], function(require, exports) {
  "use strict";
  var analytics = require('jira/analytics');

  exports.trigger = function (analyticKey, payload) {
    analytics.send({name: analyticKey, data: payload || {}});
  };
});
