define('dndattachment/progressbars/ProjectUploadProgressBar', ['require'], function(require) {
    var $ = require('jquery');
    var UploadProgressBar = require('dndattachment/progressbars/UploadProgressBar');
    return UploadProgressBar.extend({
        getProjectId: function() {
            return this.$node.parents('form').find('*[name=pid]').val();
        },

        getUploadParams: function(file) {
            var uploadParams = UploadProgressBar.prototype.getUploadParams.apply(this, arguments);

            if(this.getProjectId()) {
                uploadParams.projectId = this.getProjectId();
            }

            return uploadParams;
        },

        loadThumbnail: function(file) {

            if (!this.isImageType()) {
                var $thumbnailNode = this.getThumbnailNode();
                $thumbnailNode
                    .removeClass('upload-progress-bar__thumbnail_image')
                    .addClass('upload-progress-bar__thumbnail_icon')
                    .addClass('upload-progress-bar__thumbnail_icon_aui');

                // Determine which icon and what alt title to set depending on attachment's mime type
                var typeIconCss = 'aui-iconfont-devtools-file';
                var typeIconTitle = 'File';
                if (typeof file === 'object') {
                    typeIconCss = JIRA.Templates.ViewIssue.matchFileClass( { mimetype : file.type } );
                    typeIconTitle = JIRA.Templates.ViewIssue.matchFileIconAlt( { mimetype : file.type } );
                }

                var $thumbnail = $('<span class="aui-icon aui-icon-large"/>')
                    .addClass(typeIconCss)
                    .attr('title', typeIconTitle);

                return $thumbnail.appendTo($thumbnailNode);

            } else {
                return this._super.apply(this, arguments);
            }


        }
    });
});