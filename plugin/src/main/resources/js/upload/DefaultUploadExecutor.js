/**
 * This is the default executor that uploads the files when none of the other registered executors hit
 */
define('dndattachment/upload/default/executor', ['require', 'exports'], function(require, exports) {
    'use strict';
    exports.name = 'Default attachment executor';

    /**
     * The default executor's weight is always 0. As in it is always the lowest thing
     */
    exports.weight = 0;

    /**
     * This executor is always valid
     */
    exports.isValid = function(event, args) {
        return true;
    };

    exports.processFiles = function(files, attachmentDropZone) {
        return attachmentDropZone.uploadFiles(files);
    };
});
