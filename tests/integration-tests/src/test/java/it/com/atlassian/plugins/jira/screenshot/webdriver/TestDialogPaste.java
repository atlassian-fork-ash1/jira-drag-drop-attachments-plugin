package it.com.atlassian.plugins.jira.screenshot.webdriver;

import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.components.JiraHeader;
import com.atlassian.jira.pageobjects.config.DisableRTE;
import com.atlassian.jira.pageobjects.dialogs.quickedit.AbstractIssueDialog;
import com.atlassian.jira.pageobjects.dialogs.quickedit.CreateIssueDialog;
import com.atlassian.jira.pageobjects.dialogs.quickedit.ExtendedEditIssueDialog;
import com.atlassian.jira.pageobjects.elements.GlobalMessage;
import com.atlassian.jira.pageobjects.model.DefaultIssueActions;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.testkit.client.restclient.SearchRequest;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.test.categories.OnDemandAcceptanceTest;
import it.com.atlassian.plugins.jira.screenshot.pageobjects.PasteEventEmulator;
import it.com.atlassian.plugins.jira.screenshot.pageobjects.ViewIssuePage;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.openqa.selenium.By;

import javax.annotation.Nullable;

import static com.atlassian.jira.functest.framework.suite.Category.WEBDRIVER_TEST;
import static junit.framework.Assert.assertEquals;
import static org.apache.commons.lang3.StringUtils.trimToEmpty;

@WebTest({ WEBDRIVER_TEST })
@Category ({ OnDemandAcceptanceTest.class })
public class TestDialogPaste extends BasePasteWebTest
{
    private static final By SELECT_DESCRIPTION = By.cssSelector("#edit-issue-dialog form #description-wiki-edit textarea");
    private static final By SELECT_FORM = By.tagName("form");
    private static final By SELECT_DIALOG_CONTENT = By.className("jira-dialog-content");

    @Test
    public void testCreateIssueDialog()
    {
        final String issueSummary = "issue summary";
        final Tracer tracer = traceContext.checkpoint();
        final CreateIssueDialog createIssueDialog = pageBinder.bind(JiraHeader.class).createIssue();
        Poller.waitUntilTrue("CreateIssueDialog was not opened.", createIssueDialog.isOpen());
        switchToFullMode(createIssueDialog, tracer);
        createIssueDialog.fill("summary", issueSummary);

        performPasteToDialog(createIssueDialog.find(SELECT_DIALOG_CONTENT), SELECT_FORM);
        createIssueDialog.submit(GlobalMessage.class);

        final String issueKey = getIssueKey(issueSummary);
        goToIssuePage(issueKey);
        verifyFileWasAttached();
        verifyFileInDescription(issueKey, false);
    }

    @Test
    @DisableRTE
    public void testEditIssueDialog()
    {
        jira.goToViewIssue(issueKey).getIssueMenu().invoke(DefaultIssueActions.EDIT_ISSUE);
        final Tracer tracer = traceContext.checkpoint();
        final ExtendedEditIssueDialog editIssueDialog = pageBinder.bind(ExtendedEditIssueDialog.class);
        if (!editIssueDialog.isInFullMode())
        {
            switchToFullMode(editIssueDialog, tracer);
        }

        performPasteToDialog(editIssueDialog.find(SELECT_FORM), SELECT_DESCRIPTION);
        editIssueDialog.submit();

        verifyFileWasAttached();
        verifyFileInDescription(issueKey, true);
    }

    private void performPasteToDialog(final PageElement dialogElement, @Nullable By targetElement)
    {
        PasteEventEmulator.emulatePaste(targetElement != null ? dialogElement.find(targetElement) : dialogElement, FILE_NAME);
        Poller.waitUntilTrue(dialogElement.find(By.cssSelector(".upload-progress-bar__upload-ready")).timed().isVisible());
    }

    private void verifyFileWasAttached()
    {
        Poller.waitUntilTrue(finder.find(By.cssSelector("#attachmentmodule a[href*=image-]")).timed().isVisible());
    }

    private void verifyFileInDescription(final String issueKey, boolean expected)
    {
        ViewIssuePage issuePage = pageBinder.bind(ViewIssuePage.class, issueKey);
        PageElement descriptionEdit = issuePage.goToDescriptionEdit();
        boolean match = trimToEmpty(descriptionEdit.getValue()).matches("!image-.*png\\|thumbnail!");
        assertEquals("Screenshot markup inserted to comment field", expected, match);
    }

    private String getIssueKey(String issueSummary) {
        return getBackdoor().search().getSearch(new SearchRequest().jql(String.format("text~\"%s\"", issueSummary))).issues.get(0).key;
    }

    private void goToIssuePage(String issueKey) {
        jira.goToViewIssue(issueKey);
    }

    private void switchToFullMode(AbstractIssueDialog issueDialog, Tracer tracer)
    {
        issueDialog.switchToFullMode();
        traceContext.waitFor(tracer, "jira.issue.dnd.issuedropzone.render");
    }
}
