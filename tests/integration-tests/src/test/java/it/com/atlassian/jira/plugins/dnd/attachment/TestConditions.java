package it.com.atlassian.jira.plugins.dnd.attachment;

import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.plugins.dnd.attachment.pageobjects.AttachmentsDropZone;
import com.atlassian.pageobjects.ProductInstance;
import org.junit.After;
import org.junit.Test;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.suite.Category.WEBDRIVER_TEST;
import static org.junit.Assert.assertTrue;

@WebTest ({ WEBDRIVER_TEST })
public class TestConditions extends BaseWebdriverTest
{
    @Inject
    protected TraceContext traceContext;

    @Inject
    protected ProductInstance jiraProduct;

    @After
    public void tearDown()
    {
        backdoor.attachments().enable();
    }

    @Test
    public void testConditionDisabled() throws Exception
    {
        backdoor.attachments().disable();
        JIRA.goToViewIssue(issueKey);
        assertTrue("drop zone is not available", !pageBinder.bind(AttachmentsDropZone.class).isAvailable());
    }

    @Test
    public void testConditionEnabled() throws Exception
    {
        backdoor.attachments().enable();
        JIRA.goToViewIssue(issueKey);
        assertTrue("drop zone is visible", pageBinder.bind(AttachmentsDropZone.class).isVisible());
    }
}
