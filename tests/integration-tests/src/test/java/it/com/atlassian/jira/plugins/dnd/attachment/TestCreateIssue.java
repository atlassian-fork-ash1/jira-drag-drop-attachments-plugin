package it.com.atlassian.jira.plugins.dnd.attachment;

import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.components.JiraHeader;
import com.atlassian.jira.pageobjects.components.fields.AutoComplete;
import com.atlassian.jira.pageobjects.components.fields.MultiSelectSuggestion;
import com.atlassian.jira.pageobjects.dialogs.quickedit.CreateIssueDialog;
import com.atlassian.jira.pageobjects.pages.CreateIssuePage;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.plugins.dnd.attachment.pageobjects.AttachmentsSection;
import com.atlassian.jira.plugins.dnd.attachment.pageobjects.CreateIssueDetailsPage;
import com.atlassian.jira.plugins.dnd.attachment.pageobjects.CreateIssueDialogDropZone;
import com.atlassian.jira.plugins.dnd.attachment.pageobjects.CreateIssuePageDropZone;
import com.atlassian.jira.plugins.dnd.attachment.pageobjects.DropZone;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.test.categories.OnDemandAcceptanceTest;
import com.google.common.io.Files;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.inject.Inject;
import java.util.List;

import static com.atlassian.jira.functest.framework.suite.Category.WEBDRIVER_TEST;
import static com.atlassian.jira.pageobjects.framework.util.JiraLocators.body;
import static com.atlassian.jira.plugins.dnd.attachment.pageobjects.DropZone.DRAGOVER_CLASS;
import static com.atlassian.pageobjects.elements.query.Poller.waitUntilTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@WebTest ({ WEBDRIVER_TEST })
@Category ({ OnDemandAcceptanceTest.class })
public class TestCreateIssue extends BaseWebdriverTest
{
    @Inject
    private TraceContext traceContext;

    @Inject
    private PageElementFinder elementFinder;

    @Test
    public void testCreateIssueDialogRetainAttachmentsOnIssueTypeChange() throws Exception
    {
        final CreateIssueDialog createIssueDialog = pageBinder.bind(JiraHeader.class).createIssue();
        createIssueDialog.selectProject(PROJECT_NAME);
        waitUntilTrue("CreateIssueDialog was not opened.", createIssueDialog.isOpen());

        DropZone dropZone = pageBinder.bind(CreateIssueDialogDropZone.class);
        dropZone.dropImage(TEST_FILE_NAME);

        assertFalse("Body does not have dragover class", elementFinder.find(body()).hasClass(DRAGOVER_CLASS));

        List<String> images = dropZone.getFileNames();
        assertEquals("One image on upload list", 1, images.size());
        assertTrue("File name starts with '" + Files.getNameWithoutExtension(TEST_FILE_NAME) + "'", images.get(0).startsWith(Files.getNameWithoutExtension(TEST_FILE_NAME)));
        assertTrue("File name ends with '" + Files.getFileExtension(TEST_FILE_NAME) + "'", images.get(0).endsWith(Files.getFileExtension(TEST_FILE_NAME)));

        final Tracer tracer = traceContext.checkpoint();
        createIssueDialog.selectIssueType(secondaryIssueType.getName());
        traceContext.waitFor(tracer, "jira.issue.dnd.issuedropzone.render");

        dropZone = pageBinder.bind(CreateIssueDialogDropZone.class);

        images = dropZone.getFileNames();
        assertEquals("One image on upload list", 1, images.size());
        assertTrue("File name starts with '" + Files.getNameWithoutExtension(TEST_FILE_NAME) + "'", images.get(0).startsWith(Files.getNameWithoutExtension(TEST_FILE_NAME)));
        assertTrue("File name ends with '" + Files.getFileExtension(TEST_FILE_NAME) + "'", images.get(0).endsWith(Files.getFileExtension(TEST_FILE_NAME)));
    }

    @Test
    public void testCreateIssueDialogRetainAttachmentsOnModeToggle() throws Exception
    {

        final CreateIssueDialog createIssueDialog = pageBinder.bind(JiraHeader.class).createIssue();
        waitUntilTrue("CreateIssueDialog was not opened.", createIssueDialog.isOpen());

        DropZone dropZone = pageBinder.bind(CreateIssueDialogDropZone.class);
        dropZone.dropImage(TEST_FILE_NAME);

        createIssueDialog.switchToCustomMode().addFields("attachment");

        List<String> images = dropZone.getFileNames();
        assertEquals("One image on upload list", 1, images.size());
        assertTrue("File name starts with '" + Files.getNameWithoutExtension(TEST_FILE_NAME) + "'", images.get(0).startsWith(Files.getNameWithoutExtension(TEST_FILE_NAME)));
        assertTrue("File name ends with '" + Files.getFileExtension(TEST_FILE_NAME) + "'", images.get(0).endsWith(Files.getFileExtension(TEST_FILE_NAME)));

        Tracer tracer = traceContext.checkpoint();
        createIssueDialog.switchToFullMode();
        traceContext.waitFor(tracer, "jira.issue.dnd.issuedropzone.render");

        dropZone = pageBinder.bind(CreateIssueDialogDropZone.class);

        images = dropZone.getFileNames();
        assertEquals("There is still only one image on upload list", 1, images.size());
        assertTrue("File name starts with '" + Files.getNameWithoutExtension(TEST_FILE_NAME) + "'", images.get(0).startsWith(Files.getNameWithoutExtension(TEST_FILE_NAME)));
        assertTrue("File name ends with '" + Files.getFileExtension(TEST_FILE_NAME) + "'", images.get(0).endsWith(Files.getFileExtension(TEST_FILE_NAME)));
    }

    @Test
    public void testCreateIssuePage() throws Exception
    {
        final CreateIssuePage createIssuePage = JIRA.goTo(CreateIssuePage.class);
        final AutoComplete projectQuickSearch = createIssuePage.getProjectQuickSearch();
        final MultiSelectSuggestion testProjectOption =
                projectQuickSearch.clearQuery().query(PROJECT_NAME).getActiveSuggestion();
        projectQuickSearch.acceptUsingKeyboard(testProjectOption);
        createIssuePage.submit();

        final CreateIssueDetailsPage createIssueDetailsPage = pageBinder.bind(CreateIssueDetailsPage.class);

        DropZone dropZone = pageBinder.bind(CreateIssuePageDropZone.class);
        dropZone.dropImage(TEST_FILE_NAME);

        List<String> images = dropZone.getFileNames();
        assertEquals("One image on upload list", 1, images.size());
        assertTrue("File name starts with '" + Files.getNameWithoutExtension(TEST_FILE_NAME) + "'", images.get(0).startsWith(Files.getNameWithoutExtension(TEST_FILE_NAME)));
        assertTrue("File name ends with '" + Files.getFileExtension(TEST_FILE_NAME) + "'", images.get(0).endsWith(Files.getFileExtension(TEST_FILE_NAME)));

        // submit page and check if attachments persisted after page reload
        createIssueDetailsPage.submit();

        dropZone = pageBinder.bind(CreateIssuePageDropZone.class);

        images = dropZone.getFileNames();
        assertEquals("One image on upload list", 1, images.size());
        assertTrue("File name starts with '" + Files.getNameWithoutExtension(TEST_FILE_NAME) + "'", images.get(0).startsWith(Files.getNameWithoutExtension(TEST_FILE_NAME)));
        assertTrue("File name ends with '" + Files.getFileExtension(TEST_FILE_NAME) + "'", images.get(0).endsWith(Files.getFileExtension(TEST_FILE_NAME)));

        createIssueDetailsPage.setSummary("Please add d&d to create issue page.");
        createIssueDetailsPage.submit();

        final List<String> fileNames = pageBinder.bind(AttachmentsSection.class).getFileNames();
        assertEquals("There is one attachment", 1, fileNames.size());
        assertTrue("File name starts with '" + Files.getNameWithoutExtension(TEST_FILE_NAME) + "'", fileNames.get(0).startsWith(Files.getNameWithoutExtension(TEST_FILE_NAME)));
        assertTrue("File name ends with '" + Files.getFileExtension(TEST_FILE_NAME) + "'", fileNames.get(0).endsWith(Files.getFileExtension(TEST_FILE_NAME)));
    }

    @Test
    public void testCancelAttachment()
    {
        final CreateIssuePage createIssuePage = JIRA.goTo(CreateIssuePage.class);
        createIssuePage.getProjectQuickSearch().clearQuery().query(PROJECT_NAME).getActiveSuggestion().click();
        createIssuePage.submit();

        final CreateIssueDetailsPage createIssueDetailsPage = pageBinder.bind(CreateIssueDetailsPage.class);
        createIssueDetailsPage.setSummary("Please add d&d to create issue page.");

        final DropZone dropZone = pageBinder.bind(CreateIssuePageDropZone.class);
        dropZone.dropImage(TEST_FILE_NAME);

        dropZone.getProgressBar(TEST_FILE_NAME).cancel();

        createIssueDetailsPage.submit();
        final List<String> fileNames = pageBinder.bind(AttachmentsSection.class).getFileNames();
        assertEquals("There are attachments", 0, fileNames.size());
    }
}
