var file = new File([arguments[1]], arguments[2]);
var event = jQuery.Event("drop");
event.dataTransfer = {files: [file]};
jQuery(arguments[0]).find('.issue-drop-zone__target').trigger(event);
jQuery(arguments[0]).find('.issue-drop-zone__target')[0].scrollIntoView();
