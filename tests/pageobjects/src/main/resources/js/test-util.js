// Drop event emulator
// this code will be executed inside function
// so ignore IntelliJ warning about arguments

var evt = jQuery.Event( "drop" );
evt.dataTransfer = {files: [ document.createElement('canvas').mozGetAsFile(arguments[1]) ]};
jQuery(arguments[0]).find('.issue-drop-zone__target').trigger(evt);
jQuery(arguments[0]).find('.issue-drop-zone__target')[0].scrollIntoView();
