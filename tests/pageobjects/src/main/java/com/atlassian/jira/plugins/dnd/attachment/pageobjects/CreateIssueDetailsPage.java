package com.atlassian.jira.plugins.dnd.attachment.pageobjects;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;

import static com.atlassian.pageobjects.elements.query.Conditions.and;

/**
 * The "create issue" page that has the drop zone.
 */
public class CreateIssueDetailsPage extends AbstractJiraPage {

    @ElementBy (id = "issue-create-submit")
    private PageElement submit;

    @ElementBy (id = "summary")
    private PageElement summary;

    @Override
    public TimedCondition isAt()
    {
        // Testing for the submit button is not enough, because CreateIssuePage also has one with its ID
        return and(submit.timed().isVisible(), summary.timed().isVisible());
    }

    @Override
    public String getUrl()
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    public void setSummary(String summary) {
        this.summary.type(summary);
    }

    public void submit() {
        this.submit.click();
    }
}
