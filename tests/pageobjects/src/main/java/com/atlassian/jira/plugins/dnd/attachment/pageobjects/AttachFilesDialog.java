package com.atlassian.jira.plugins.dnd.attachment.pageobjects;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.Poller;
import com.atlassian.pageobjects.elements.query.TimedCondition;
import com.google.inject.Inject;

public class AttachFilesDialog {

    public static final String SUBMITTING_CLASS = "submitting";

    @ElementBy(cssSelector = "form#attach-file")
    private PageElement attachFileDialog;

    @ElementBy(id = "attach-file-submit")
    private PageElement submit;

    @ElementBy(id = "attach-file-cancel")
    private PageElement cancel;

    @Inject
    private PageBinder pageBinder;

    public void close() {
        cancel.click();
        Poller.waitUntilFalse("Dialog should be closed", isOpen());
    }

    public void submit() {
        submit.click();
        Poller.waitUntilFalse("Dialog should finish submitting", attachFileDialog.timed().hasClass(SUBMITTING_CLASS));
    }

    public TimedCondition isOpen() {
        return attachFileDialog.timed().isPresent();
    }

    public DropZone getDropZone() {
        return pageBinder.bind(AttachFilesDropZone.class);
    }
}
