package com.atlassian.jira.plugins.dnd.attachment.pageobjects;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.jira.pageobjects.pages.viewissue.ViewIssuePage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;

public class EditIssueDetailsPage extends AbstractJiraPage {
    @ElementBy (id = "issue-edit-submit")
    PageElement submit;
    @ElementBy (id = "summary")
    PageElement summary;

    private final String issueKey;
    private final String issueId;

    public EditIssueDetailsPage(String issueKey, String issueId) {
        this.issueKey = issueKey;
        this.issueId = issueId;
    }

    @Override
    public TimedCondition isAt()
    {
        return submit.timed().isVisible();
    }

    @Override
    public String getUrl()
    {
        return String.format("secure/EditIssue!default.jspa?id=%s", issueId);
    }

    public void setSummary(String summary) {
        this.summary.type(summary);
    }

    public void submit() {
        this.submit.click();

        pageBinder.bind(ViewIssuePage.class, issueKey);
    }
}
