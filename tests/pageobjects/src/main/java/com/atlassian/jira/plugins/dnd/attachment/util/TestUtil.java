package com.atlassian.jira.plugins.dnd.attachment.util;

import com.atlassian.jira.pageobjects.util.UserSessionHelper;
import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;

public class TestUtil {
    public static String readResource(String name)
    {
        final ClassLoader loader = UserSessionHelper.class.getClassLoader();
        final InputStream resourceAsStream = loader.getResourceAsStream(name);
        try
        {
            return IOUtils.toString(resourceAsStream, "utf-8");
        }
        catch (IOException e)
        {
            throw new RuntimeException(e);
        }
        finally
        {
            IOUtils.closeQuietly(resourceAsStream);
        }
    }
}
